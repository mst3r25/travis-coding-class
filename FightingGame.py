import random

class Character:

    def __init__(self, name, health, attack, defense):
        self.name = name
        self.health = health
        self.attack = attack
        self.defense = defense

#    @property
#    def health(self):
#        return self.__health

    @property
    def attack(self):
        return self.__attack

    @attack.setter
    def attack(self, attack):
        self.__attack = attack

    @property
    def defense(self):
        return self.__defense

    @defense.setter
    def defense(self, defense):
        self.__defense = defense

    def __str__(self):
        return ("{} has a max health of {}, an attack of {}, and a defense of {}"\
                .format(self.name, self.health, self.attack, self.defense))

    def dealDamage(self, enemy):
        damageMulitplier = 100 / (100 + enemy.defense)
        damageDealt = int(self.attack * damageMulitplier)
        enemy.health -= damageDealt
        print("{} dealt {} damage to {}. Leaving {} with {} health" \
              .format(self.name, damageDealt, enemy.name, enemy.name, enemy.health))


def main():
    # Team A
    rory = Character("Rory", 250, 10, 20)
    travis = Character("Travis", 250, 20, 5)
    bob = Character("Bob", 250, 10, 20)
    jill = Character("Jill", 250, 20, 10)
    karen = Character("Karen", 250, 20, 10)
    dylan = Character("dylan", 200, 25, 15)
    gibby = Character("gibby", 300, 15, 5)

    teamA = [rory, travis, bob, jill, karen, dylan, gibby]

    # Team B

    doug = Character("Doug", 250, 15, 15)
    april = Character("April", 250, 20, 10)
    todd = Character("Todd", 250, 15, 25)
    sally = Character("Sally", 250, 30, 5)
    michelle = Character("Michelle", 250, 5, 30)
    teamB = [doug, todd, sally, michelle, april]

    teamA_Curr = 0
    teamB_Curr = 0
    teamA_fighter = teamA[teamA_Curr]
    teamB_fighter = teamB[teamB_Curr]

    # TODO Make this smaller, this is still hardcoded and implement speeds.
    while teamA_Curr < len(teamA) and teamB_Curr < len(teamB):
        teamA_fighter.dealDamage(teamB_fighter)

        if teamB_fighter.health <= 0:
            print("{} lost".format(teamB_fighter.name))
            teamB_Curr += 1
            if teamB_Curr >= len(teamB):
                break
            teamB_fighter = teamB[teamB_Curr]

        teamB_fighter.dealDamage(teamA_fighter)

        if teamA_fighter.health <= 0:
            print("{} lost".format(teamA_fighter.name))
            teamA_Curr += 1
            if teamA_Curr >= len(teamA):
                break
            teamA_fighter = teamA[teamA_Curr]

    if teamA_Curr >= len(teamA) and teamB_Curr >= len(teamB):
        print("We have a draw")
    elif teamA_Curr >= len(teamA):
        print("Team A lost")
    elif teamB_Curr >= len(teamB):
        print("Team B lost")


    # for fighter in range(len(teamA)):
    #     while teamB[fighter].health > 0 and teamA[fighter].health > 0:
    #         teamA[fighter].dealDamage(teamB[fighter])
    #         teamB[fighter].dealDamage(teamA[fighter])
    #
    #     if teamB[fighter].health and teamA[fighter].health <= 0:
    #         print("We have a draw!")
    #     elif teamB[fighter].health <= 0:
    #         #print(random.choice(teamB), "got knocked out!")
    #         print("{} has won".format(teamA[fighter].name))
    #     elif random.choice(teamA).health <= 0:
    #         #print(random.choice(teamA), "got knocked out!")
    #         print("{} has won".format(teamB[fighter].name))



#print("{} has {} health left.".format(travis.name, travis.health))

main()